//Zrobione z pomocą Pawła Leji.

#include <iostream>

using namespace std;

struct Wektor {
	int x;
	int y;
};

void DodajWektory(Wektor *w1, Wektor *w2, Wektor *wynik) {
	wynik->x = w1->x + w2->x;
	wynik->y = w1->y + w2->y;
}

int MnozWektory(Wektor *w1, Wektor *w2) {
	return ((w1->x * w2->x) + (w1->y * w2->y));
}

void WyswietlWektor(Wektor *w) {
	cout << "[" << (w->x) << "," << (w->y) << "]";
}

int main() {
	Wektor *Wektor1 = new Wektor;
	Wektor *Wektor2 = new Wektor;
	Wektor *Wektor3 = new Wektor;


	cout << "Podaj wspolrzedna X dla I wektora: ";
	cin >> Wektor1->x;
	cout << "Podaj wspolrzedna Y dla I wektora: ";
	cin >> Wektor1->y;

	cout << "Podaj wspolrzedna X dla II wektora: ";
	cin >> Wektor2->x;
	cout << "Podaj wspolrzedna Y dla II wektora: ";
	cin >> Wektor2->y;
	cout << endl;
	cout << "Wspolrzedne [x,y] Twoich wektorow to: " << endl;
	cout << "Wektor I = "; WyswietlWektor(Wektor1);
	cout << endl;
	cout << "Wektor II = "; WyswietlWektor(Wektor2);
	cout << endl;

	DodajWektory(Wektor1, Wektor2, Wektor3);

	cout << "Wynik dodawania skalarnego Twoich wektorow = "; WyswietlWektor(Wektor3); cout << endl;
	cout << endl;
	cout << "Wynik mnozenia skalarnego Twoich wektorow =  " << MnozWektory(Wektor1, Wektor2) << endl;
	cout << endl;
	system("pause");
	return 0;
}
